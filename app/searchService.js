// search service
angular
    .module('newApp')
    .service('search', search);

search.$inject = ['$http'];

function search($http) {
    return {
        getData: getData
    };

    function getData(term) {
        return $http({
                url: 'search/' + term,
                method: 'GET',
            })
            .then(getDataComplete)
            .catch(getDataFailed);

        function getDataComplete(response) {
            return response
        }

        function getDataFailed(error) {
            console.log(error)
        }
    }
}


