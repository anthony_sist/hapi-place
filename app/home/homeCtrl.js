(function() {
    angular.module('newApp').controller('homeCtrl', homeCtrl)
    homeCtrl.$inject = ['search', '$scope'];

    function homeCtrl(search, $scope) {
        //Assign viewmodel
        var vm = this;
        vm.doSearch = doSearch;
		vm.socketMessage = 'Not Connected';
		
		
        function doSearch(term) {
            return search.getData(term).then(function(data) {
                vm.searchResponse = data;
                return vm.searchResponse;
            });
        }
        //get comment feed socket
        var socket = io(':8080');
        socket.on('connected', function(data) {
            vm.socketMessage = data;
			$scope.$apply();
        })
        
    }
})();