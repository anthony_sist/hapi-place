module.exports = function(Server) {
    /**
     *  Route to query anything
     */
    Server.route({
        method: 'GET',
        path: '/search/{search}',
        handler: function(req, reply) {
            reply('You hit the search api!');
        }
    });
};