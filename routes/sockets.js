exports.register = function(server, options, next) {
    var io = require('socket.io').listen(server.select('feed').listener, {
        log: false
    });
    io.sockets.on('connection', function(socket) {
        socket.emit('connected', 'You are connect to a socket!');
    });
};
exports.register.attributes = {
    name: 'socket plugin'
};